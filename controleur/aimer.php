<?php

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $racine = "..";
}
include_once "$racine/modele/bd.aimer.inc.php";


// recuperation des donnees GET, POST, et SESSION
$idR = $_GET["idR"];

$mailU = getMailULoggedOn();
if ($mailU != "") {
    $aimer = getAimerById($mailU, $idR);
    
    if($aimer) {
        deleteAimer($mailU, $idR);
    }
    else{
        addAimer($mailU, $idR);
    }      
}

// redirection vers le referer
header('Location: ' . $_SERVER['HTTP_REFERER']);
?>
