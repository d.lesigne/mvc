<?php

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $racine = "..";
}


include_once "$racine/modele/bd.utilisateur.inc.php";
include_once "$racine/modele/bd.typecuisine.inc.php";
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.aimer.inc.php";

    $mailU = getMailULoggedOn();
    
    if(isset ($_POST['pseudoU'])){
        updatePseudo($mailU, $_POST['pseudoU']);
    }
    else if (isset($_POST['mdpU1'])&&isset($_POST['mdpU2'])){
        if($_POST['mdpU1']==$_POST['mdpU2']){
            updateUtilisateur($mailU, $_POST['mdpU1']);
        }
    }
    else if(isset($_POST['lstidR'])){
        for($i=0;$i<count($_POST['lstidR']);$i++){
            deleteAimer($mailU, $_POST['lstidR'][$i]);
        }
    }else if(isset($_POST['delLstidTC'])){
        for($i=0;$i<count($_POST['delLstidTC']);$i++){
            deleteTypeCuisine($mailU, $_POST['delLstidTC'][$i]);
        }
    }else if(isset($_POST['addLstidTC'])){
        for($i=0;$i<count($_POST['addLstidTC']);$i++){
            addTypeCuisine($mailU, $_POST['addLstidTC'][$i]);
        }
    }
    
    $mesRestosAimes = getAimerByMailU($mailU);
    
    $CuisineAime = getTypesCuisinePreferesByMailU($mailU);
    $CuisineAimePas = getTypesCuisineNonPreferesByMailU($mailU);
    
    // appel du script de vue qui permet de gerer l'affichage des donnees
    $titre = "Modif profil";
    include "$racine/vue/entete.html.php";
    include "$racine/vue/vueUpdProfil.php";
    include "$racine/vue/pied.html.php";


?>