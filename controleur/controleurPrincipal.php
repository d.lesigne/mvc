<?php

function controleurPrincipal($action) {
    $lesActions = array();
    $lesActions["defaut"] = "listeRestos.php";
    $lesActions["liste"] = "listeRestos.php";
    $lesActions["connexion"] = "connexion.php";
    $lesActions["deconnexion"] = "deconnexion.php";
    $lesActions["recherche"] = "rechercheResto.php";
    $lesActions["detail"] = "detailResto.php";
    $lesActions["profil"] = "monProfil.php";
    $lesActions["supprimerCritique"] = "detailResto.php";
    $lesActions["cgu"] = "cgu.php";
    $lesActions["aimer"] = "aimer.php";
    $lesActions["inscription"] = "inscription.php";
    $lesActions["noter"] = "noter.php";
    $lesActions["updProfil"] = "updProfil.php";

    if (array_key_exists($action, $lesActions)) {
        return $lesActions[$action];
    } else {
        return $lesActions["defaut"];
    }
}

?>
