<?php

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $racine = "..";
}
include_once "$racine/modele/bd.critiquer.inc.php";

$idR = $_GET["idR"];

$mailU = getMailULoggedOn();
if ($mailU != "") {
    if (getCritiquerByUserAndIdr($idR, $mailU)){
        deleteCritiquer($mailU, $idR);
    }
   
    $note = $_GET["note"];
    addCritiquer($mailU, $idR, $note);  
}

// redirection vers le referer
header('Location: ' . $_SERVER['HTTP_REFERER']);
?>
