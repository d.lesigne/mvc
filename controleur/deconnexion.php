<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $racine="..";
}
include_once "$racine/modele/authentification.inc.php";

if(isLoggedOn()){
    logout();
    $titre = "Deconnexion";
    include "$racine/vue/entete.html.php";
    include "$racine/vue/vueDeconnexion.php";
    include "$racine/vue/pied.html.php";
}
?>