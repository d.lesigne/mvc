<?php

include_once "bd.inc.php";

function getCritiquerByIdR($idR) {
    $resultat = array();
    
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select mail, note, commentaire from site_mvc.critiquer where id_r=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
       
    return $resultat;
}

function getCritiquerByUserAndIdr($idR,$mailU) {
    $resultat = array();
    
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select mail, note, commentaire from site_mvc.critiquer where id_r=:idR and mail=:mailU");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
       
    return $resultat;
}

function addCritiquer($mailU, $idR,$note) {
    $resultat = -1;
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("insert into site_mvc.critiquer values (:idR, :mailU, :note,'')");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->bindValue(':note', $note, PDO::PARAM_INT);
        
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function deleteCritiquer($mailU, $idR) {
    $resultat = -1;
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("delete from site_mvc.critiquer where id_r=:idR and mail=:mailU");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getNoteMoyenneByIdR($idR) {
    
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select sum(note)/count(note)::float as moyenne from site_mvc.critiquer where id_r=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    if ($resultat["moyenne"]==NULL){
        $resultat["moyenne"] = 0;
    }
    return $resultat["moyenne"];
}


?>
