
<h1>Liste des restaurants</h1>
<?php //print_r($listeRestos);
?>
<?php
for ($i = 0; $i < count($listeRestos); $i++) {
    $lesPhotos = getPhotosByIdR($listeRestos[$i]['id']);
    ?>
    
    <div class="card">
        <div class="photoCard">
            <img src="photos/<?php echo $lesPhotos[0]["chemin"]; ?>" alt="photo" />
        </div>
        <div class="descrCard">
        <a href='./?action=detail&idR=<?= $listeRestos[$i]['id']; ?> '><?= $listeRestos[$i]['nom'] ?></a>
        <br />
            <?= $listeRestos[$i]["num_adr"] ?>
            <?= $listeRestos[$i]["voie_adr"] ?>
            <br />
            <?= $listeRestos[$i]["cp"] ?>
            <?= $listeRestos[$i]["ville"] ?>
        </div>
        <div class="tagCard">
            <ul id="tagFood">		
            </ul>
        </div>
    </div>

    <?php
}
?>


